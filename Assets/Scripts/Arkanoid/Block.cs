﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
	public int lives;
	[SerializeField] Material oneLive;
	[SerializeField] Material twoLives;
	[SerializeField] Material threeLive;

	// Use this for initialization
	void Awake () {
		changeColor ();
	}

	private void changeColor(){
		Material[] mats = GetComponent<Renderer>().materials;
		if (lives == 1) {
			mats [0] = oneLive;
		} else if (lives == 2) {
			mats [0] = twoLives;
		} else if (lives == 3) {
			mats [0] = threeLive;
		}
		GetComponent<Renderer> ().materials = mats;
	}
	
	public void Touch(){
		lives--;

		if (lives <= 0) {
			gameObject.SetActive (false);
		} else {
			changeColor ();
		}
	}
}
