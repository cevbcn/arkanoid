﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallArkanoid : MonoBehaviour {

	public Vector2 speed;

	private Vector2 iniSpeed;
	private Vector3 iniPos;

	public int maxSpeed;

	// Use this for initialization
	void Start () 
	{
		iniSpeed = speed;
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Bounds") 
		{
			speed.x *= -1f;
		}else if (other.tag == "Player") {
			changeVelocity (other.gameObject.transform);
		} else if (other.tag == "Goal") {
			speed.y *= -1f;
		}else if (other.tag == "LimitUp") {
			speed.y *= -1f;
		} else if (other.tag == "Dead") {
			speed.y *= -1f;
			//Reset ();
		} else if (other.tag == "Block") {
			speed.y *= -1f;

			other.gameObject.GetComponent<Block> ().Touch ();
			//Reset ();
		}
	}

	void changeVelocity(Transform player){
		//La bola va hacia la derecha
		speed.y*=-1;

		if (speed.x > 0) {
			//Posicion X del player superior a la bola (Frenamos la bola)
			if (player.position.x >= transform.position.x) {
				speed.x = speed.x * 0.5f;
			}else{
				//Posicion X del player inferior a la bola (Incrementamos la velocidad)
				speed.x = speed.x * 1.5f;
			}
		} else { //La bola va hacia la izquierda
			//Posicion X del player superior a la bola (Incrementamos la velocidad)
			if (player.position.x >= transform.position.x) {
				speed.x = speed.x * 1.5f;
			}else{
				//Posicion X del player inferior a la bola (Frenamos la bola)
				speed.x = speed.x * 0.5f;
			}
		}
		if (speed.x > maxSpeed) {
			speed.x = maxSpeed;
		} else if (speed.x < -maxSpeed) {
			speed.x = -maxSpeed;
		}

		if (speed.y > maxSpeed) {
			speed.y = maxSpeed;
		} else if (speed.y < -maxSpeed) {
			speed.y = -maxSpeed;
		}
	}

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
