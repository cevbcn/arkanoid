﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArkanoid : MonoBehaviour {

	private Vector3 initPos;

	public float speed;
	public float limit;
	private float move;

	void Start()
	{
		initPos = transform.position;
	}

	void Update () 
	{
		move = Input.GetAxis("Horizontal") * speed;
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(move, 0, 0);

		if (transform.position.x < -limit) {
			transform.position = new Vector3 (-limit, transform.position.y, transform.position.z);
		}else if(transform.position.x > limit) {
			transform.position = new Vector3 (limit, transform.position.y, transform.position.z);
		}
	}
}
